import { MoshprojectPage } from './app.po';

describe('moshproject App', function() {
  let page: MoshprojectPage;

  beforeEach(() => {
    page = new MoshprojectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
