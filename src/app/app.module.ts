import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { AngularFireModule } from 'angularfire2'

import 'hammerjs';

import { AppComponent } from './app.component';

export const firebaseConfig = {
    apiKey: "AIzaSyBDdzXeQdludR6Q5925VgUIr7KUiHcPfcE",
    authDomain: "moshproject-b848d.firebaseapp.com",
    databaseURL: "https://moshproject-b848d.firebaseio.com",
    storageBucket: "moshproject-b848d.appspot.com",
    messagingSenderId: "302252946936"
    };

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
