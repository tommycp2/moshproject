import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'app works!';
  cuisines: FirebaseListObservable<any>;

  

  constructor(private af: AngularFire) {
  }

 
  ngOnInit() {
    this.cuisines = this.af.database.list('/cuisines');

  }

  add() {
    this.cuisines.push({
      name: "Asian",
      address: {
        street: "303 Mooloolah",
        postcode: 4554
      }

    }
    )

  }

}
